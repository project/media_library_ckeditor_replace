<?php

namespace Drupal\media_library_ckeditor_replace\Plugin\CKEditorPlugin;

// Class in the middle for backwards compatibility with Drupal < 9.5.
if (class_exists('Drupal\ckeditor\Plugin\CKEditorPlugin\DrupalMedia')) {
  class DrupalMedia extends \Drupal\ckeditor\Plugin\CKEditorPlugin\DrupalMedia {}
}
else {
  class DrupalMedia extends \Drupal\media\Plugin\CKEditorPlugin\DrupalMedia {}
}
