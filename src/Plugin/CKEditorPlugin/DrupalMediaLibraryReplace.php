<?php

namespace Drupal\media_library_ckeditor_replace\Plugin\CKEditorPlugin;

use Drupal\editor\Entity\Editor;

/**
 * Defines the "drupalmedialibraryreplace" plugin.
 *
 * @CKEditorPlugin(
 *   id = "drupalmedialibraryreplace",
 *   label = @Translation("Replace embedded media with the Media Library"),
 * )
 *
 * @internal
 *   Plugin classes are internal.
 */
class DrupalMediaLibraryReplace extends DrupalMedia {

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [
      'drupalmedialibrary',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->moduleExtensionList->getPath('media_library_ckeditor_replace') . '/js/plugins/drupalmedialibraryreplace/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCssFiles(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

}
