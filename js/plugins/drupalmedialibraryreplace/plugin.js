(function (Drupal, CKEDITOR) {
  Drupal.theme.mediaEmbedReplaceButton = function () {
    return '<button class="media-library-item__replace">' + Drupal.t('Replace media') + '</button>';
  };

  CKEDITOR.plugins.add('drupalmedialibraryreplace', {
    requires: 'drupalmedialibrary',
    afterInit: function afterInit(editor) {
      // Inject the Replace button by hijacking the setup method for the Edit
      // button and inserting first the Replace button and then calling the
      // original method for adding the Edit button.
      var _setUpEditButton = editor.widgets.registered.drupalmedia._setUpEditButton;

      editor.widgets.registered.drupalmedia._setUpEditButton = function() {
        if (this.element.findOne('.media-embed-error')) {
          return;
        }

        var isElementNode = function isElementNode(n) {
          return n.type === CKEDITOR.NODE_ELEMENT;
        };

        var embeddedMediaContainer = this.data.hasCaption ? this.element.findOne('figure') : this.element;
        var embeddedMedia = embeddedMediaContainer.getFirst(isElementNode);

        if (this.data.link) {
          embeddedMedia = embeddedMedia.getFirst(isElementNode);
        }
        var replaceButton = CKEDITOR.dom.element.createFromHtml(Drupal.theme('mediaEmbedReplaceButton'));
        embeddedMedia.getFirst().insertBeforeMe(replaceButton);

        var widget = this;
        this.element.findOne('.media-library-item__replace').on('click', function (event) {
          var saveCallback = function saveCallback(values) {
            event.cancel();
            editor.fire('saveSnapshot');

            // Exchange the UUID only while keeping all other attributes.
            var widgetData = CKEDITOR.tools.clone(widget.data);
            widgetData.attributes['data-entity-uuid'] = values.attributes['data-entity-uuid'];
            widget.setData('attributes', widgetData.attributes);

            editor.fire('saveSnapshot');
          };

          Drupal.ckeditor.openDialog(editor, editor.config.DrupalMediaLibrary_url, {}, saveCallback, editor.config.DrupalMediaLibrary_dialogOptions);
        });

        this.element.findOne('.media-library-item__replace').on('keydown', function (event) {
          var returnKey = 13;

          var spaceBar = 32;
          if (typeof event.data !== 'undefined') {
            var keypress = event.data.getKey();
            if (keypress === returnKey || keypress === spaceBar) {
              event.sender.$.click();
            }

            event.data.$.stopPropagation();
            event.data.$.stopImmediatePropagation();
          }
        });

        _setUpEditButton.apply(this, arguments);
      };
    }
  });
})(Drupal, CKEDITOR);